# Case Study DevOps PT Bangun Inovasi Teknologi (BIT House)

---

This repository was created to contain the case study devops for PT Bangun Inovasi Teknologi (BIT House).

#### System requirements

The requirements for running this application are as follows:

- Docker
- Composer

#### How to run?

- Copy or change the name `.env.example` to `.env` then fill in the required keys such as `DB_DATABASE, DB_USERNAME, DB_PASSWORD` and adjust them to your configuration,
- Run `docker-compose build app` in the `root directory` of this project,
- Run the application with docker compose `docker-compose up -d`,
- Run the composer to install all dependencies `docker-compose exec app composer install`,
- Generate application key `docker-compose exec app php artisan key:generate`.

To show information about the state of your active services, run: `docker-compose ps`
